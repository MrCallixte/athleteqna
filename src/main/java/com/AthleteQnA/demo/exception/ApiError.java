package com.AthleteQnA.demo.exception;

import java.util.*;

import org.springframework.http.HttpStatus;
import org.springframework.validation.Errors;

public class ApiError {
	
	@Override
	public String toString() {
		return "ApiError [status= " + status + ", message=" + message + ", errors=" + errors + "]";
	}
	private HttpStatus status;
	private String message;
	private List<String> errors;

public ApiError(HttpStatus status, String message, List<String> errors) {
	super();
	this.status = status;
	this.message = message;
	this.errors =errors;
}
public ApiError(HttpStatus status, String message, String error) {
	super();
	this.status = status;
	this.message = message;
	errors = Arrays.asList(error);
}
public ApiError(){
	super();
}
public HashMap<String, Object> getMap(Errors errors){
		Map<String, Object> Hm = new HashMap<>();
		List<String> err = new ArrayList<>();
		errors.getFieldErrors().forEach(fieldError ->err.add(fieldError.getDefaultMessage()));
		Hm.put("Status", HttpStatus.BAD_REQUEST);
		Hm.put("errors", err);
		return (HashMap<String, Object>) Hm;
}
}