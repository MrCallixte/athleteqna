package com.AthleteQnA.demo.Service;

/*

2010-01-02

 */
import com.AthleteQnA.demo.Model.Answer;
import com.AthleteQnA.demo.Repository.AnswerRepository;
import com.AthleteQnA.demo.Repository.QuestionRepository;

import com.AthleteQnA.demo.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service
public class AnswerService {
	
	@Autowired
    private QuestionRepository questionRepository;

	@Autowired
	private AnswerRepository answerRepository;

    public Answer answerQuestion(Long questionId, Answer answer) {
        return questionRepository.findById(questionId).map(question -> {
            question.setAnswered(true);
            question.setAnswer(answer);
            return answerRepository.save(answer);
        }).orElseThrow(() -> new ResourceNotFoundException("Can't answer question with  id:  " + questionId + ". It does not exist"));
    
    }
    public Answer getOneAnswer(Long questionId) {

        if (!questionRepository.existsById(questionId)) {

            throw new ResourceNotFoundException("Question with id " + questionId + " doesn't exist, so does its answer.");
        }else if(!questionRepository.findById(questionId).get().isAnswered()){
            throw new ResourceNotFoundException("Question with id " + questionId + " has not been answered yet");
        }
        
        return questionRepository.findById(questionId).get().getAnswer();
    }
    public ResponseEntity<?> deleteAnswer(Long questionId) {

        if(!questionRepository.existsById(questionId)){
            throw new ResourceNotFoundException("Question with id " + questionId + " doesnt' exists." +
                                             "You are trying to delete an answer of a non existent question");
        }

        return questionRepository.findById(questionId).map(question -> {

         if(question.isAnswered() == false){
             throw new ResourceNotFoundException("Question with id " + questionId + "has not been answered yet ");
         }
        question.setAnswered(false);
        answerRepository.delete(question.getAnswer());
        question.setAnswer(null);
        return ResponseEntity.ok().build();

        }).orElseThrow(() -> new ResourceNotFoundException("QuestionId " + questionId + "is not found"));

    }

}
