package com.AthleteQnA.demo.Service;


import com.AthleteQnA.demo.Repository.QnASessionRepository;
import com.AthleteQnA.demo.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.AthleteQnA.demo.Model.Question;
import com.AthleteQnA.demo.Repository.QuestionRepository;

import java.util.Optional;

@Service
public class QuestionService {
	
	@Autowired
	private QuestionRepository questionRepository;

	@Autowired
	private QnASessionRepository qnasessionRepository;

	public Page<Question> findByQnASessionId(Long qaId, Pageable pageable) {

		if(!qnasessionRepository.existsById(qaId)){

			throw new ResourceNotFoundException("Q&A Session with id " + qaId + "is not found");
		}
		
		return questionRepository.findByQnasessionId(qaId,pageable);
	}

	public ResponseEntity<?> createQuestion(Long qaId, Question question){
		return qnasessionRepository.findById(qaId).map( Qnasession -> {
			question.setQnasession(Qnasession);

			return new ResponseEntity(questionRepository.save(question), HttpStatus.OK);

		}).orElseThrow(() -> new ResourceNotFoundException("Q&A Session with id  " + qaId + "is not found"));

	}

	public Question updateQuestion(Long qaId, Long id, Question question) {
		if(!qnasessionRepository.existsById(qaId)){
			throw new ResourceNotFoundException("Q&A Session with id  " + qaId + "is not found");
		}

		return questionRepository.findById(id).map(LocalQuestion -> {
			LocalQuestion.setAskedBy(question.getAskedBy());
			LocalQuestion.setText(question.getText());
			return questionRepository.save(LocalQuestion);

		}).orElseThrow(() -> new ResourceNotFoundException("Question with id " + id + " is not found"));
	}

	public ResponseEntity<?> deleteQuestionByIdAndQnASessionId(Long qaId, Long id) {

		return questionRepository.findByIdAndQnasessionId(id,qaId).map(each -> {
			questionRepository.delete(each);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("Question with id " + id + " is not found"));
	}

	public Optional<Question> getOneQuestion(Long qaId, Long id) {

		return questionRepository.findByIdAndQnasessionId(id,qaId);
	}

	public Page<Question> findByQnasessionAndAnswered(Long qaId, String answered, Pageable pageable){

		if(!qnasessionRepository.existsById(qaId)){

			throw new ResourceNotFoundException("Q&A Session with id " + qaId + " is not found");
		}

		return questionRepository.findByQnasessionIdAndIsAnswered(qaId,Boolean.valueOf(answered),pageable);
	}
}

