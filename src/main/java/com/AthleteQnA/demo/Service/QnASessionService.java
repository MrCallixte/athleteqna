package com.AthleteQnA.demo.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.AthleteQnA.demo.exception.ResourceNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.AthleteQnA.demo.Model.QnASession;
import com.AthleteQnA.demo.Repository.QnASessionRepository;


@Service
public class QnASessionService {

	@Autowired
	private QnASessionRepository qnaSessionRepository;

	public List<QnASession> geAllQnASessions() {
		List<QnASession> qnaSessions = new ArrayList<>();
		qnaSessionRepository.findAll()
				.forEach(qnaSessions::add);
		return qnaSessions;
	}

	public Optional<QnASession> getQnASession(Long id) {
		return qnaSessionRepository.findById(id);

	}

	public void addQnASession(QnASession qnaSession) {

		qnaSessionRepository.save(qnaSession);
	}

	public ResponseEntity<?> deleteQnASession(Long id) {
		return qnaSessionRepository.findById(id).map(QnA -> {
			qnaSessionRepository.delete(QnA);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("Q&A Session with id " + id + " is not found"));
	}

	public Page<QnASession> getQnASession(Pageable pageable) {

		return qnaSessionRepository.findAll(pageable);

	}

	public QnASession createQnASession(QnASession qnasession) {

		return qnaSessionRepository.save(qnasession);
	}

	public QnASession updateQnASession(Long qaId, QnASession qnasession) {
		return qnaSessionRepository.findById(qaId).map(QnA -> {
			QnA.setDescription(qnasession.getDescription());
			QnA.setEndTime(qnasession.getEndTime());
			QnA.setHostUser(qnasession.getHostUser());
			QnA.setStartTime(qnasession.getStartTime());

			return qnaSessionRepository.save(QnA);
		}).orElseThrow(()-> new ResourceNotFoundException("Q&A Session with id " + qaId + " not found "));
	}
}
