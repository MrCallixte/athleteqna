package com.AthleteQnA.demo.Controller;


import com.AthleteQnA.demo.exception.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import com.AthleteQnA.demo.Model.Question;
import com.AthleteQnA.demo.Service.QuestionService;

import javax.validation.Valid;

@RestController
public class QuestionController {
	
	@Autowired
	private QuestionService questionService;

	@GetMapping("/qa/{qaId}/questions")
	public Page<Question> getAllQuestionByQnASessionId(@RequestParam(required=false) String answered, @PathVariable Long qaId,Pageable pageable){

		return answered == null? questionService.findByQnASessionId(qaId, pageable):questionService.findByQnasessionAndAnswered(qaId, answered,pageable);
	}
	@PostMapping("/questions/{qaId}")
	public ResponseEntity<?> createQuestion(@PathVariable Long qaId, @Valid @RequestBody Question question, Errors errors){
		if(errors.hasErrors()) {
			return new ResponseEntity<>(new ApiError().getMap(errors), HttpStatus.BAD_REQUEST);
		}else
		return new ResponseEntity<>(questionService.createQuestion(qaId, question), HttpStatus.CREATED);
	}
	@GetMapping("/qa/{qaId}/questions/{id}")
	public ResponseEntity<?> getOneQuestion(@PathVariable Long qaId, @PathVariable Long id){

		return questionService.getOneQuestion(qaId,id).isEmpty()? new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK): new ResponseEntity<>(questionService.getOneQuestion(qaId, id), HttpStatus.OK);
	}
	@PutMapping("/qa/{qaId}/questions/{id}")
	public ResponseEntity<?> updateQuestion(@PathVariable Long qaId, @PathVariable Long id, @Valid @RequestBody Question question,Errors errors){

		if(errors.hasErrors()) {
			return new ResponseEntity<>(new ApiError().getMap(errors), HttpStatus.BAD_REQUEST);

		}else return new ResponseEntity<>(questionService.updateQuestion(qaId, id, question), HttpStatus.OK);
	}

	@DeleteMapping("/qa/{qaId}/questions/{id}")
	public ResponseEntity<?> deleteQuestion(@PathVariable Long qaId, @PathVariable Long id){

		return questionService.deleteQuestionByIdAndQnASessionId(qaId, id);
	}
}
