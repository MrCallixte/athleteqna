package com.AthleteQnA.demo.Controller;

import com.AthleteQnA.demo.Model.Answer;
import com.AthleteQnA.demo.Service.AnswerService;
import com.AthleteQnA.demo.exception.ApiError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AnswerController {

	
    @Autowired
    AnswerService answerService;

    @PostMapping("/answers/{questionId}")
    public ResponseEntity<?> answerQuestion(@PathVariable Long questionId, @Valid @RequestBody Answer answer, Errors errors){
        if(errors.hasErrors()){

            return new ResponseEntity<>(new ApiError().getMap(errors), HttpStatus.BAD_REQUEST);
        }else
       return new ResponseEntity<>(answerService.answerQuestion(questionId, answer), HttpStatus.OK);

    }
    @GetMapping("/answers/{questionId}")
    public ResponseEntity<?> getOneAnswer(@PathVariable Long questionId){

        return answerService.getOneAnswer(questionId) == null? new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK):new ResponseEntity<>(answerService.getOneAnswer(questionId), HttpStatus.OK);

    }
    @DeleteMapping("/answers/{questionId}")
    public ResponseEntity<?> deleteAnswer(@PathVariable Long questionId){
        return answerService.deleteAnswer(questionId);
    }

}
