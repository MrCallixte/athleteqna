package com.AthleteQnA.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import com.AthleteQnA.demo.Model.QnASession;
import com.AthleteQnA.demo.Service.QnASessionService;
import com.AthleteQnA.demo.exception.ApiError;


import javax.validation.Valid;
import java.util.*;

@RestController
public class AthleteQnAController {

	// End points for Q/A
	@Autowired
	private QnASessionService qnaService;

	@RequestMapping(value = "/qa", method = RequestMethod.GET, produces="application/json")
	public Page<QnASession> getAllQnASessions(Pageable pageable){
		return qnaService.getQnASession(pageable);
	}
	
	@ResponseBody
	@RequestMapping(value = "/qa/{qaId}",method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<?> getoneQnASession(@PathVariable Long qaId){
		return qnaService.getQnASession(qaId).isEmpty()? new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK): new ResponseEntity<>(qnaService.getQnASession(qaId), HttpStatus.OK);
	}
	
	@PostMapping("/qa")
	@ResponseBody
	public ResponseEntity<?> createQnASession(@Valid @RequestBody QnASession qnasession, Errors errors){
		if(errors.hasErrors()){
			Map<String, Object> m = new ApiError().getMap(errors);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}else
			return new ResponseEntity<>(qnaService.createQnASession(qnasession),HttpStatus.CREATED);
	}

	@PutMapping("/qa/{qaId}")
	public QnASession updateQnASession(@PathVariable Long qaId, @Valid @RequestBody QnASession qnasession){
		return qnaService.updateQnASession(qaId,qnasession);
	}
	@DeleteMapping("/qa/{qaId}")
	public ResponseEntity<?> deleteQnASession(@PathVariable Long qaId){
		return new ResponseEntity<>(qnaService.deleteQnASession(qaId), HttpStatus.OK);
	}
}
