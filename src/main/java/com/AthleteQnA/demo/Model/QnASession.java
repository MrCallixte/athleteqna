package com.AthleteQnA.demo.Model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Entity
public class QnASession {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank(message="Description must not be null")
	private String description;
	
	@NotBlank(message="Host name must be not null")
	private String hostUser;
	
	@NotNull(message="Start Time name must be not blank")
	private LocalDate startTime;
	@NotNull(message="End Time must be not null")
	private LocalDate endTime;

	@OneToMany(mappedBy = "qnasession", fetch = FetchType.LAZY,orphanRemoval = true)
	List<Question> questionsList = new ArrayList<>();

	public QnASession() {}

	public QnASession(Long id, String hostUser, LocalDate startTime, LocalDate endTime, String description) {
		super();
		this.id = id;
		this.hostUser = hostUser;
		this.startTime = startTime;
		this.endTime = endTime;
		this.description = description;
	}

	@Override
	public String toString() {
		return "QnASession{" +
				"id=" + id +
				", description='" + description + '\'' +
				", hostUser='" + hostUser + '\'' +
				", startTime=" + startTime +
				", endTime=" + endTime +
				'}';
	}

	public String getDescription() { return description;}

	public void setDescription(String description) { this.description = description; }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHostUser() {
		return hostUser;
	}

	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}

	public LocalDate getStartTime() {
		return startTime;
	}

	public void setStartTime(LocalDate startTime) {
		this.startTime = startTime;
	}

	public LocalDate getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalDate endTime) {
		this.endTime = endTime;
	}

	}

