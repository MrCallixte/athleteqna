package com.AthleteQnA.demo.Model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@AnswerRequestAnnotation
public class Answer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnoreProperties
	private Long id;
	
	private String text;
	private String imageUrl;
	private String answeredBy;

	public Answer() {}
	
	public Answer(String text, String imageUrl, String answeredBy) {
		super();
	
		this.text = text;
		this.imageUrl = imageUrl;
		this.answeredBy = answeredBy;
	}
	@Override
	public String toString() {
		return "Answer{" +
				"id=" + id +
				", text='" + text + '\'' +
				", imageUrl='" + imageUrl + '\'' +
				", answeredBy='" + answeredBy + '\'' +

				'}';
	}

	public Long getId() {
		return id;
	}



	public Long getAnswerId() {
		return id;
	}
	public void setAnswerId(Long answerId) {
		this.id = answerId;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getAnsweredBy() {
		return answeredBy;
	}
	public void setAnsweredBy(String answeredBy) {
		this.answeredBy = answeredBy;
	}
	
	

}
