package com.AthleteQnA.demo.Model;

import com.AthleteQnA.demo.Service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AnswerValidator implements ConstraintValidator<AnswerRequestAnnotation, Answer> {

	@Autowired
	AnswerService answerService;

	@Override
    public void initialize(AnswerRequestAnnotation constraintAnnotation) {

    }
	@Override
	public boolean isValid(Answer answer, ConstraintValidatorContext context) {

		if (answer == null || answer.getAnsweredBy().trim().isEmpty() ||(answer.getText().isEmpty() && answer.getImageUrl().isEmpty()))
			return false;
			
		return true;
	}

}
