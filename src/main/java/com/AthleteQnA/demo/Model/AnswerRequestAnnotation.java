package com.AthleteQnA.demo.Model;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = AnswerValidator.class)
@Target({ ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnswerRequestAnnotation {
    String message() default "{answered_by is required and either text and/or image_url  are required}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
