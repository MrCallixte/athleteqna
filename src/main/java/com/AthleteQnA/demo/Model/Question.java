package com.AthleteQnA.demo.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;


@Entity
public class Question {
	
	@Id
	@JsonIgnoreProperties
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank(message="question text must not be empty")
	private String text;
	@NotBlank(message="Please provide the askedBy field")
	private String askedBy;
	private Boolean isAnswered=false;

	@JsonIgnore
	@ManyToOne
	private QnASession qnasession;

	@OneToOne(fetch=FetchType.LAZY,cascade = CascadeType.ALL, orphanRemoval = true)
	  @JoinTable(
	      name="QSN_ANS",
	      joinColumns=
	        @JoinColumn(name="QSN_ADD", referencedColumnName="id"),
	      inverseJoinColumns=
	        @JoinColumn(name="ANS_ID", referencedColumnName="id", unique = true))

	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Answer answer;
	

	@Override
	public String toString() {
		return "Question{" +
				"id=" + id +
				", text='" + text + '\'' +
				", askedBy='" + askedBy + '\'' +
				", isAnswered=" + isAnswered +
				", qnasession=" + qnasession +
				
				'}';
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public boolean isAnswered() {
		return isAnswered;
	}


	public void setAnswered(boolean answered) {
		isAnswered = answered;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		
		this.id = id;
	}

	public QnASession getQnasession() {
		return qnasession;
	}

	public void setQnasession(QnASession qnasession) {

		this.qnasession = qnasession;
	}

	public Long getQuestionID() {
		return id;
	}
	public void setQuestionID(Long questionID) {
		this.id = questionID;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getAskedBy() {
		return askedBy;
	}
	public void setAskedBy(String askedBy) {
		this.askedBy = askedBy;
	}


	public Question(Long questionID, String text, String askedBy, Long qnAId) {
		super();
		this.id = questionID;
		this.text = text;
		this.askedBy = askedBy;
		this.qnasession = new QnASession(qnAId,"",null,null,"");
	}
	public Question() {}
	

}
