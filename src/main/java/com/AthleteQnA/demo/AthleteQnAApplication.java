package com.AthleteQnA.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackageClasses = AthleteQnAApplication.class)

public class AthleteQnAApplication {

	public static void main(String[] args) {
		SpringApplication.run(AthleteQnAApplication.class, args);
	}

}
