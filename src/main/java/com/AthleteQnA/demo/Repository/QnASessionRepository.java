package com.AthleteQnA.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.AthleteQnA.demo.Model.QnASession;

public interface QnASessionRepository extends JpaRepository<QnASession, Long> {



}
