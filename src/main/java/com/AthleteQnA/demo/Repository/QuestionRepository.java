package com.AthleteQnA.demo.Repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.AthleteQnA.demo.Model.Question;

import java.util.Optional;

public interface QuestionRepository  extends JpaRepository<Question, Long> {

    Page<Question> findByQnasessionId(Long qaId, Pageable pageable);

    Optional<Question> findByIdAndQnasessionId(Long id, Long qaId);

    Page<Question> findByQnasessionIdAndIsAnswered(Long qaId, Boolean isAnswered, Pageable page);

}
