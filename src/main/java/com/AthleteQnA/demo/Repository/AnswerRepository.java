package com.AthleteQnA.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.AthleteQnA.demo.Model.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Long> {

}
