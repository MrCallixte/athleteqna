#README

## System requirements
* Java 1.8+
* Maven 3.5 +
* Apache Derby 10.14.2.0

## Updated worktest file location
```http://localhost:8080/backend-qa-worktest.html```

To run the server, please clone the project file located at: 
    ```https://bitbucket.org/MrCallixte/athleteqna/src/master/```
    
  and while at the root of the project type within your command line terminal: 
  ```mvn spring-boot:run``` to start the server on port 8080. 
 * You can also run the project by downloading Spring Tools 4+ IDE, open the project and run the project. 

## Design overview and discussion
  
### Design overview
   
* I chose to use 3 tables for the database:
    * QnASession(id: Long PK, description: String, hostUser: String, startTime: LocalDate,endTime: LocalDate)
    * Question(id: Long PK,text: String, askedBy: String, QnASession_id: Long FK)
    * Answer(id: Long PK, text: String, imageUrl: String, answeredBy: String,question_id: Long FK)
   
   ***PK refers to Primary Key and FK refers to foreign key***
   
* I chose to use embedded database: ```Apache Derby 10.14.2.0``` for simplicity, and ease of the database configuration.
    
        
### Limitations
 
 * One limitation is that the stored data only persists during the lifecycle of the application running. 
 This is due to the fact that the embedded database is created at runtime and destroyed as soon as the application is stopped.
 * I did not provide the unit test for all classes, and in reality this is a requirement for all the classes. 
 * Pagination was not fully tested with big data set. 
 * I didn't create any user table, therefore the application doesn't support any fetch/query that gravitates around users. 
 
 
### Discussion
 
 * I have modified the work sheet accordingly, and you could try to use Postman as well. 
 * To retrieve a list of questions by answered filter, use postman and follow below pattern
 ```http://localhost:8080/qa/1/questions?answered=true```. This end point is fetching a list of answered questions in Q&A Session with id 1. For non-answered questions, simply change from ```answered=true``` to ```answered=false```  

### Improvements

* Use external database for data persistence after the application stops
* User table in case there is a need to find questions asked by a particular user,answered by particular user,Q&A hosted by user,...
* Unit test for all classes, as well as integration test.
* Testing pagination with big data set  
 
 *Please feel free to contact me if you have any questions to run the application at *nacalli07@gmail.com*